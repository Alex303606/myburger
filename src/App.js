import React, {Component} from 'react';
import BurgerBuilder from "./containers/BurgerBuilder/BurgerBuilder";
import {Route, Switch} from "react-router-dom";
import CheckOut from "./containers/CheckOut/CheckOut";
import Layout from "./components/Layout/Layout";
import Orders from "./containers/Orders/Orders";

class App extends Component {
	render() {
		return (
			<Layout>
				<Switch>
					<Route path="/" exact component={BurgerBuilder}/>
					<Route path="/orders" component={Orders}/>
					<Route path="/checkout" component={CheckOut}/>
					<Route render={() => <h1>404 not found</h1>}/>
				</Switch>
			</Layout>
		);
	}
}

export default App;
