import React from 'react'
import './OrderItem.css'

const OrderItem = props => {
	
	const ingredients = [];
	
	for (let ingredientName in props.ingredients) {
		ingredients.push({
			name: ingredientName,
			amount: props.ingredients[ingredientName]
		});
	}
	const ingredientsOutPut = ingredients.map(ig => (
		<span key={ig.name}>{ig.name} ({ig.amount})</span>
	));
	return (
		<div className="OrderItem">
			<p>Ingredients: {ingredientsOutPut}</p>
			<p>price: <strong>{props.price} KGS</strong></p>
		</div>
	);
};
export default OrderItem;