import React, {Component, Fragment} from 'react'
import CheckOutSummary from "../../components/Order/CheckOutSummary/CheckOutSummary";
import {Route} from "react-router-dom";
import ContactData from "./ContactData/ContactData";

class CheckOut extends Component {
	state= {
		ingredients: {},
		price: 0
	};
	
	componentDidMount(){
		const query = new URLSearchParams(this.props.location.search);
		const ingredients = {};
		let price = 0;
		console.log(query.entries());
		for(let param of query.entries()) {
			if(param[0] === 'price') {
				price = param[1];
			} else {
				ingredients[param[0]] = parseInt(param[1],10);
			}
		}
		this.setState({ingredients,price});
	}
	
	checkOutCancelledHandler = () => {
		this.props.history.goBack();
	};
	
	checkOutContinuedHandler = () => {
		this.props.history.replace('/checkout/contact-data');
	};
	
	render(){
		return (
		<Fragment>
			<CheckOutSummary
				ingredients={this.state.ingredients}
				checkOutCancelled={this.checkOutCancelledHandler}
				checkOutContinued={this.checkOutContinuedHandler}
			/>
			<Route
				path={this.props.match.path + '/contact-data'}
				render={props => (
					<ContactData
						{...props}
						price={this.state.price}
						ingredients={this.state.ingredients}
					/>
				)}
			/>
		</Fragment>
		)
	}
}

export default CheckOut;