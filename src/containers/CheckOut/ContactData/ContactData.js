import React, {Component} from 'react';
import './ContactData.css';
import Button from "../../../components/UI/Button/Button";
import axios from '../../../axios-orders'
import Spinner from "../../../components/UI/Spinner/Spinner";
class ContactData extends Component {
	
	state = {
		name: '',
		email: '',
		street: '',
		postal: '',
		loading: false
	};
	
	valueChanged = (e) => {
		const name = e.target.name;
		this.setState({[name]: e.target.value});
	};
	
	orderHandler = (e) => {
		e.preventDefault();
		this.setState({loading: true});
		const order = {
			ingredients: this.props.ingredients,
			price: this.props.price,
			customer: {
				name: this.state.name,
				email: this.state.email,
				street: this.state.street,
				postal: this.state.postal
			}
		};
		
		axios.post('/order.json',order).finally(()=>{
			this.setState({loading: false});
			this.props.history.push('/');
		});
	};
	
	render() {
		let form = (
			<form>
				<input onChange={(e) => this.valueChanged(e)} value={this.state.name} className="Input" type="text" name="name"
				       placeholder="Your Name"/>
				<input onChange={(e) => this.valueChanged(e)} value={this.state.email} className="Input" type="email" name="email"
				       placeholder="Your Mail"/>
				<input onChange={(e) => this.valueChanged(e)} value={this.state.street} className="Input" type="text" name="street"
				       placeholder="Street"/>
				<input onChange={(e) => this.valueChanged(e)} value={this.state.postal} className="Input" type="text" name="postal"
				       placeholder="Postal Code"/>
				<Button btnType="Success" clicked={this.orderHandler}>ORDER</Button>
			</form>
		);
		
		if(this.state.loading) {
			form = <Spinner/>;
		}
		return (
			<div className="ContactData">
				<h4>Enter your Contact Data</h4>
				{form}
			</div>
		);
	}
}

export default ContactData;