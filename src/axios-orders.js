import axios from 'axios'

const instance = axios.create({
	baseURL: 'https://reactburgeralexeyostrikov.firebaseio.com/'
});

export default instance;